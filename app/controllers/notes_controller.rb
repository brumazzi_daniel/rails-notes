class NotesController < ApplicationController
  before_action :set_note, only: [:show, :edit, :update, :destroy]

  def index
    @notes = Note.where(user_id: current_user.id)
    @notes = @notes.where('title like ?', "%#{params[:title]}%") unless params[:title].blank?
    @notes = @notes.where('content like ?', "%#{params[:content]}%") unless params[:content].blank?
  end

  def new
    @note = Note.new
  end

  def edit
  end

  def create
    @note = Note.new(note_params)
    @note.user_id = current_user.id

    respond_to do |format|
      if @note.save
        format.html { redirect_to notes_path, notice: 'Nota Criada' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @note.update(note_params)
        format.html { redirect_to notes_path, notice: 'Nota Atualizada' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @note.destroy
    respond_to do |format|
      format.html { redirect_to notes_url, notice: 'Note foi apagada.' }
    end
  end

  private
    def set_note
      @note = Note.find_by_id(params[:id].to_i)
    end

    # Only allow a list of trusted parameters through.
    def note_params
      params.require(:note).permit(:title, :content, :date, :priority)
    end
end
