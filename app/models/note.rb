class Note < ApplicationRecord
  enum priority: [:low, :middle, :high]

  def priority_to_s
    I18n.t("model.note.#{self.priority.to_s}")
  end

  def priority_to_i
    Note.priorities[self.priority.to_s]
  end
  
  def priority_to_c
    return 'primary' if self.low?
    return 'warning' if self.middle?
    return 'danger' if self.high?
  end

  def self.priority_to_list
    list = {}
    Note.priorities.each do |p, v|
      list.store I18n.t("model.note.#{p}"), p
    end
    return list
  end
end
